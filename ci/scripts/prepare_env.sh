#/usr/bin/sh

set -x
set -e

echo '--ENV SETUP START--'

echo '--Prepare submodules--'
git submodule init
git submodule update

echo '--Copy deps--'
cp -R /opt/deps/* ./
cp -f ./ci/.env ./

# Preparar database
service mysql start

echo '--Creating Databases--'
echo 'CREATE DATABASE exampleDB;' | mysql -uroot -proot

echo '--Loading Model--'
cat ./app/assets/databases/exampleDB/database.sql | mysql -uroot -proot exampleDB

echo '--Loading Data--'
cat ./ci/databases/exampleDB.test.sql | mysql -uroot -proot exampleDB

echo '--Updating Model--'
cat ./app/assets/databases/exampleDB/changes.sql | sed 's/^--//' | mysql -uroot -proot exampleDB

echo '--ENV SETUP COMPLETE--'
