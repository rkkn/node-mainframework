"use strict"

const Assert = require('assert');
const Request = require('supertest');
const Path = require('path');

//const App = require(Path.join(__dirname, '../..', 'server'));
const Config = require(Path.join(__dirname, '../../app', 'config'));
const API = Request(`http://localhost:${Config.port}`);

describe('ExampleRouter', function () {
    let createdElementId;

    describe('/', function () {
        describe('GET', function () {
            it('Should return text as default data format', function (done) {
                API.get('/')
                .expect('Content-Type', /text/)
                .expect(200, done);
            });
        });
    });

    describe('/v1/example/examples', function () {
        let path = this.title;

        describe('GET', function () {
            it('Should return json as default data format', function (done) {
                API.get(path)
                .expect('Content-Type', /json/)
                .expect(200)
                .then(res => {
                    //Process response here with res.body object
                })
                .then(done);
            });
        });

        describe('POST', function () {
            it('Should return 400 status code', function (done) {
                API.post(path)
                .send({})
                .expect('Content-Type', /json/)
                .expect(400, done);
            });

            it('Should return 400 status code', function (done) {
                API.post(path)
                .send({ example: {} })
                .expect('Content-Type', /json/)
                .expect(400, done);
            });

            it('Should return 200 status code', function (done) {
                API.post(path)
                .send({ example: { field1: getRandomInt(0, 100), field2: getRandomInt(0, 100) } })
                .expect('Content-Type', /json/)
                .expect(200)
                .then(res => {
                    createdElementId = res.body.data.id;
                })
                .then(done);
            });
        });
    });

    describe('/v1/example/examples/:example_id', function () {
        let path = this.title.replace('\/\:example_id','');

        describe('GET', function () {
            it('Should return 404 status code', function (done) {
                API.get(`${path}/A`)
                .expect('Content-Type', /json/)
                .expect(404, done);
            });

            it('Should return 404 status code', function (done) {
                API.get(`${path}/-1`)
                .expect('Content-Type', /json/)
                .expect(404, done);
            });

            it('Should return 200 status code', function (done) {
                API.get(`${path}/${createdElementId}`)
                .expect('Content-Type', /json/)
                .expect(200, done);
            });
        });

        describe('PATCH', function () {
            it('Should return 400 status code', function (done) {
                API.patch(`${path}/A`)
                .send({ example: { field1: getRandomInt(0, 100), field2: getRandomInt(0, 100) } })
                .expect('Content-Type', /json/)
                .expect(404, done);
            });

            it('Should return 400 status code', function (done) {
                API.patch(`${path}/-1`)
                .send({ example: { field1: getRandomInt(0, 100) } })
                .expect('Content-Type', /json/)
                .expect(400, done);
            });

            it('Should return 400 status code', function (done) {
                API.patch(`${path}/${createdElementId}`)
                .send({ example: { field1: getRandomInt(0, 100), field2: getRandomInt(0, 100) } })
                .expect('Content-Type', /json/)
                .expect(200, done);
            });
        });

        describe('DELETE', function () {
            it('Should return 400 status code', function (done) {
                API.delete(`${path}/A`)
                .expect('Content-Type', /json/)
                .expect(404, done);
            });

            it('Should return 400 status code', function (done) {
                API.delete(`${path}/-1`)
                .send({ example: { field1: getRandomInt(0, 100), field2: getRandomInt(0, 100) } })
                .expect('Content-Type', /json/)
                .expect(404, done);
            });
        });
    });
});

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}
