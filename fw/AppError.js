'use strict';

const Config = require('../app/config');

class AppError extends Error {
    constructor(status, msg) {
        super(`Error ${status}: ${msg}`);
        this.status = status ? status: Config.errDefaultCode;
        this.msg = msg? msg: Config.errDefaultMsg;
    }
}

module.exports = AppError;
