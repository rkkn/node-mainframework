'use strict';

//Load env file
require('dotenv').config();

//Load Config
const Config = require('./app/config');

// NPM dependencies.
const express = require('express');
const sequelize = require('sequelize');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const morgan = require('morgan');
const compression = require('compression');
const bodyParser = require('body-parser');
const path = require('path');
//const cors = require('cors');

// Initializations.
var app = express();

// App related modules.
// Import Class
const AppFw = require('./fw/AppFw');

// Use class to get appError
const AppError = AppFw.appError();

// Parse as urlencoded and json.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//app.use(cors());
app.use(compression());

// Hook up the HTTP logger.
if (process.env.NODE_ENV !== "testing") {
    app.use(morgan('tiny'));
}

// Hook up Passport.
app.use(passport.initialize());

// Hook the passport JWT strategy.
let hookJWTStrategy = require('./app/libs/passportExample.js');
hookJWTStrategy(passport);

app.all(`/v${Config.version}/*`, function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Expose-Headers', 'Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');

    next();
});


// Set the static files location.
app.use(express.static(__dirname + '/public'));

new Promise(async (resolve, reject) => {
    try {
        //Init DB (static)
        AppFw.loadDBs();

        // Bundle API routes.
        await AppFw.loadRoutes(app, passport);

        //Error handler
        app.use(`/v${Config.version}/*`, function(err, req, res, next) {
            // El handling de los errores esta en el archivo fw/AppFw.js. Se hace con la clase estatica
            AppFw.handleError(res, err);
        })

        // Catch all fail routes.
        app.get('*', function(req, res) {
            res.sendFile(path.join(__dirname + '/public/index.html'));
        });

        // Start the server.
        const server = app.listen(Config.port, function() {
            let hostname = require('os').hostname();
            console.log(`APIv${Config.version} running at http://${hostname}:${Config.port}`);
        });

        process.on('SIGTERM', () => {
            app.close(() => {
                console.log('Process terminated')
            })
        })

        resolve();
    } catch (err) {
        reject(err);
    }
}).catch(err => {
    console.log(err);
    process.exit(0);
});
