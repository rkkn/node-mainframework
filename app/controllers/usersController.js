'use strict';

const { AppController } = require('../imports');
const { ExampleDB } = require('../imports').loadDBs();

class UsersController extends AppController {
    static async getMe (req, params, query, body) {
        return req.user;
    }

    static async editMe (req, params, query, body) {
        return await req.user.update(body.user);
    }

    static async getAll (req, params, query, body) {
        return await ExampleDB.user.getAll({
            where: ExampleDB.user.where(query),
            offset: query.offset,
            limit: query.limit,
        });
    }

    static async get (req, params, query, body) {
        return await ExampleDB.user.get(params.user_id);
    }

    static async getExamples (req, params, query, body) {
        return await req.user.getExamples({
            where: ExampleDB.example.where(query),
            offset: query.offset,
            limit: query.limit,
        });
    }
}

module.exports = UsersController;
