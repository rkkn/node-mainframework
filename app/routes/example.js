'use strict';

// Router import
const { AppRouter } = require('../imports');

// Controllers import
const { AuthController, UsersController, ExampleController } = require('../imports').loadControllers();

// Class define
class ExampleRouter extends AppRouter {
    setRoutes(router, passport) {
        //ExampleRoutes - AuthController Binding
        router.post('/auth', this.pipe(AuthController.authBase));

        //Estas rutas estan mas arriba para que /examples/me no tenga conflicto con /examples/:example_id
        //ExampleRoutes - ExampleController Advanced examples
        router.get('/examples/me', passport.authenticate('jwt', { session: false }), this.pipe(ExampleController.getUserExamples));
        router.get('/me/examples', passport.authenticate('jwt', { session: false }), this.pipe(UsersController.getExamples));

        //ExampleRoutes - ExampleController Binding
        router.get('/examples', this.pipe(ExampleController.getAll));
        router.post('/examples', this.pipe(ExampleController.add));
        router.get('/examples/:example_id', this.pipe(ExampleController.get));
        router.patch('/examples/:example_id', this.pipe(ExampleController.edit));
        router.delete('/examples/:example_id', this.pipe(ExampleController.delete));

        //ExampleRoutes - UserController Binding. Autenticated routes
        router.get('/users', passport.authenticate('jwt', { session: false }), this.pipe(UsersController.getAll));
        router.get('/users/:user_id', passport.authenticate('jwt', { session: false }), this.pipe(UsersController.get));
        router.get('/me', passport.authenticate('jwt', { session: false }), this.pipe(UsersController.getMe));
        router.patch('/me', passport.authenticate('jwt', { session: false }), this.pipe(UsersController.editMe));
    }
}

// Export routes
module.exports = ExampleRouter;
