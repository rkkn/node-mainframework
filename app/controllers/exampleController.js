'use strict';

const { AppController, AppError, Op } = require('../imports');
const { ExampleDB } = require('../imports').loadDBs();
const { ExampleLib } = require('../imports').loadLibs();

class ExampleController extends AppController {
    static async getAll (req, params, query, body) {
        // Order example
        let order = [['id', 'ASC']];
        switch (query.order) {
            case "field1":
            case "field1,asc":
                order = [['field1', 'ASC']];
                break;
            case "field1,desc":
                order = [['field1', 'DESC']];
                break;
            case "custom":
                order = [['field1', 'DESC'], ['id', 'DESC']];
        }

        // Like search
        let queryFilter = AppController.likeFilter(['field1', 'field2'], query);

        return await ExampleDB.example.getAll({
            where: ExampleDB.example.where(query, queryFilter),
            offset: query.offset,
            limit: query.limit,
            order: order,
        });
    }

    static async add (req, params, query, body) {
        return await ExampleDB.example.create(body.example);
    }

    static async get (req, params, query, body) {
        if (!ExampleLib.isInt(params.example_id)) {
            throw new AppError(404, "id not allowed");
        }
        return await ExampleDB.example.getById(params.example_id);
    }

    static async edit (req, params, query, body) {
        let example = await ExampleDB.example.getById(params.example_id);
        return await example.update(body.example);
    }

    static async delete (req, params, query, body) {
        let example = await ExampleDB.example.getById(params.example_id);
        return await example.delete();
    }

    static async getUserExamples (req, params, query, body) {
        return await ExampleDB.example.getAll({
            where: ExampleDB.example.where(query, {
                user_id: req.user.id
            }),
            offset: query.offset,
            limit: query.limit,
        });
    }
}

module.exports = ExampleController;
