// Base Model.
'use strict';

const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const AppError = require('./AppError');
const AppSequelizeError = require('./AppSequelizeError');

class AppModel extends Sequelize.Model {
    // Basic associate
    static associate (Model) {
        throw Error('Implement associate ' + this.constructor.name);
    }

    // Override findAll
    static getAll (options = { where: {} }) {
        //Limpiar offset y limit en caso de ser necesario
        if (options.hasOwnProperty("offset") && (typeof options.offset !== "number" || !Number.isInteger(options.offset)))
            delete options.offset;
        if (options.hasOwnProperty("limit") && (typeof options.limit !== "number" || !Number.isInteger(options.limit)))
            delete options.limit;

        return super.findAll(options);
    }

    static getById (id, options = { where: {}}) {
        return super.findByPk(id, options)
        .then(object => {
            if ((options.noThrow? options.noThrow: false) === true) {
                return Promise.resolve(object);
            } else if (!object) {
                return Promise.reject(new AppError(404, 'Element not found'));
            } else {
                return object;
            }
        })
    }

    static findOne (options = { where: {}}) {
        return super.findOne(options)
        .then(object => {
            if ((options.noThrow? options.noThrow: false) === true) {
                return Promise.resolve(object);
            } else if (!object) {
                return Promise.reject(new AppError(404, 'Element not found'));
            } else {
                return object;
            }
        })
    }

    static findOrCreate (options = { where: {}}) {
        options.noThrow = true;
        return super.findOrCreate(options)
        .then(async ([object, created]) => {
            if (!object) {
                return Promise.reject(new AppError(500, 'Element is not created'));
            } else {
                return object;
            }
        })
    }

    static create (options = { where: {}}) {
        return super.create(options)
        .then(object => {
            if (!object) {
                return Promise.reject(new AppError(500, 'Element is not created'));
            } else {
                return object;
            }
        })
        .catch(Sequelize.ValidationError, function(err) {
            throw new AppSequelizeError("Body Model Format Error", err.errors)
        });
    }

    static update (fields, where) {
        return super.update(fields, where)
        .catch(Sequelize.SequelizeValidationError, function(err){
            throw new AppSequelizeError("Body Model Format Error", err.errors)
        });
    }

    static where (query = {}, where = {}) {
        if (where.constructor !== {}.constructor)
            throw new Error("where filter must be an object");
        return Object.assign({}, filter(query, Object.keys(this.rawAttributes)), where);
    }
}

function filter (raw, wantedKeys) {
    var filteredObject = {};
    Object.keys(raw).forEach(key => {
        if (wantedKeys.includes(key)) {
            filteredObject[key] = raw[key];
        }
    });
    return filteredObject;
}

module.exports = AppModel;
