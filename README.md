# Node MainFramework

### Requisitos
node, npm, docker, bitbucket, mysql

### Instalacion
Para instalar las dependencias, ejecutar:
```
npm install
```

### Iniciar el Server
Para iniciar el servidor, ejecutar:
```
node server.js
```

### Acceso a elementos con include.js
El archivo include.js permite tener acceso a los elementos principales de la app. Para incluir los elementos solo basta con agregar:
```
const { Config, FW, AppError, AppRouter, AppModel, AppController, AppLib, Sequelize, DataTypes, Op, loadDBs, loadControllers, loadLibs } = require('./imports');
```
Esta forma de importar los datos permite ademas poder importar solo las partes necesarias en el archivo. Por ejemplo, si solo se desea importar AppController:
```
const { AppController } = require('./imports');
```
Para evitar la redundancia cíclica al importar modelos, controladores y librerias, se implementaron los métodos __loadDBs()__, __loadControllers(folder)__, __loadLibs()__.
Es importarte evitar usar estas funciones dentro de sus respectivas carpetas asociadas. Por ejemplo, usar __loadDBs()__ dentro de app/models causaría un problema de redundacia cíclica. Y para acceder a otros modelos dentro de un modelo, se debe usar el objeto __this.sequelize.models__.

#### Accesos a DBs
Para acceder a los __Modelos__ de la BD, se utiliza la funcion __loadDBs()__. Esta funcion retorna todas las instancias a las distintas bases de datos. La funcion retorna objetos con el nombre de la carpeta en la que estan contenidos los modelos, con el nombre capitalizado. Por ejemplo, para acceder al modelo __example__ de la carpeta __app/models/exampleDB__ se haría de la siguiente forma:
```
const { ExampleDB } = require('./imports').loadDBs();
```

#### Accesos a Controladores
Para acceder a los __Controladores__ se utiliza la función __loadControllers(folder)__. Esta funcion retorna el listado de controladores que se encuentran en __app/controllers__. Por defecto el método lee en __app/controllers__, sin embargo si se entrega el parmametro folder, este permite acceder a la subcarpeta __app/controllers/{folder}__ y leer todos los controladores. Esto permite implementar multiples controladores para diferentes contextos.
```
const { ExampleController } = require('./imports').loadControllers();
```

#### Accesos a Libs
Para acceder a los __Controladores__ se utiliza la función __loadLibs()__. Esta funcion retorna el listado de controladores que se encuentran en __app/libs__.
```
const { ExampleLib } = require('./imports').loadLibs();
```

### Controladores, Modelos, Rutas, Vistas y Librerias
Este framework usa el clasico patron MVC pero añade los objetos routes para generar el link entre las rutas (endpoints), los controladores (controllers) y las vistas (views).

#### Controladores
Los controladores son utilizados para obtener y procesar la informacion. Estos estaran encargado de ir a los modelos correspondientes y generar un único objeto de salida que deberá contener toda la información necesaria para mostrar la respuesta.
Todos los métodos del controlador deben tener la siguiente estructura:
```
static async exampleMethod (req, params, query, body) {
    // req: Objeto request de Express
    // params: Objeto req.params de Express
    // query: Objeto req.query de Express pero filtrando los datos para retornar solo las llaves permitidas que se declararon en la vista. (Ver seccion vistas)
    // body: Objeto req.body de Express pero filtrando los datos para retornar solo las llaves permitidas que se declararon en la vista. (Ver seccion vistas)
    let response;
    ...
    // Buscar y generar objeto response
    ...
    return response
}
```
Esto se debe a que las rutas llaman a los métodos del controlador pasando los objetos en ese orden.

#### Modelos
Los modelos estan basados en [Sequelize](https://sequelize.org), usando promesas. En el archivo fw/AppModel.js se incluye una clase base que sirve para manejar mejor los objetos.
Los modelos se agrupan en carpetas dentro de __app/models__, donde cada una de estas carpetas generan una conexion a una base de datos.
En __config.js__ se debe declarar el objeto __Config.db.[{MODEL_FOLDER}]__ de la siguiente manera:
```
Config.db.{MODEL_FOLDER} = {
    user: "{DB_USER}",
    password: "{DB_PASSWORD}",
    name: "{DB_NAME}",
    details: {
        host: '{DB_HOST}',
        port: {DB_PORT},
        dialect: 'mysql',
        logging: false
    }
}
```
De esta forma, se puede tener acceso a multiples bases de datos.

Se implementaron los métodos getAll() y getById() para hacer busquedas, ademas se extendieron algunas clases para lanzar un throw AppError 404 en caso de que el objeto buscado no exista, sin embargo se puede incluir la llave { noThrow: true } en caso de que se desee evitar el throw error.
El método getAll() se implementó para que no ocurrieran errores con listas paginadas al pasar { limit: undefined|null } o { offset: undefined|null } puesto que remueve la llave si esta es undefined|null.
Se pueden ver los ejemplos en la carpeta __app/models/exampleBD/__

#### Rutas (routes).
Se incluye la clase AppRouter que maneja como se unen los endpoints con los controladores. Para agregar endpoints hay que crear objetos en la carpeta app/routes. Estos routers agregados se leeran automaticamente al inicio e instanciaran el acceso a las rutas definidas en el mismo, generando los endpoints bajo la ruta base __/v{VERSION}/{ROUTER.getName()}__.
Si de desea remover esta funcionalidad de ruta base, se puede usar la configuración
```
Config.routerUseBasepath = false;
```
Esta configuración permite cargar las rutas directo en el basepath __/__ y generar la documentación de las rutas en __/{ROUTER.getName()}-docs__
Para usar basePath y swaggerPath customizados, se pueden sobrecargar los métodos __getBasePath()__ y __getSwaggerPath()__ en cada archivo routes.
Ademas, como nota, los router obtienen su nombre usando la funcion __getName()__ que por defecto tiene la implementacion
```
getName () {
    return this.constructor.name.replace('Router', '').toLowerCase();
}
```
Se debe tener presente que es esta funcion la usada para ir a leer las views a su correspondientes carpeta.

##### Estructura de router.js
Los archivos router deben tener la siguiente estructura:
```
app/routes/example.js
class ExampleRouter extends AppRouter {
    setRoutes(router, passport) {
        ...
        router.get('/examples', this.pipe(ExampleController.getAll));
        ...
    }
}
```
En este ejemplo. Se crea la ruta __/examples__, que esta dirigida al método __getAll()__ de __ExampleController__.

Se pueden crear rutas protegidas con autentificacion JWT de esta manera:
```
app/routes/example.js
class ExampleRouter extends AppRouter {
    setRoutes(router, passport) {
        ...
        router.get('/examples/me', passport.authenticate('jwt', { session: false }), this.pipe(ExampleController.getUserExamples));
        ...
    }
}
```
Para mas detalle, revisar la seccion de Framework - Autentificacion.

Ademas de crear el objeto router, habrá que crear una carpeta en __app/views__ con el nombre del archivo router. Los archivos router generan la documentación automaticamente, que se cargaran dentro de la ruta __/v{VERSION}/{ROUTER.getName()}-doc__. En esta ruta estaran las entradas y las respuestas correspondientes de cada método.

##### __AppRouter.pipe(MethodCallback)__
El método AppRouter.pipe es el encargado de manejar como se realiza la consulta en el servidor. Cuando llega un request, esta funcion se encarga de filtrar las entradas segun la especificaciones que se dieron de la vista, para solo dejar pasar los parametros permitidos en los objetos __request.query__ y __request.body__. Esta funcion llama al método en el controlador especificado y luego retorna la respuesta, filtrando el objeto y devolviendo solo lo que fue especificado en la vista.
```
pipe (MethodCallback) {
    let Router = this;
    return async function (req, res, next) {
        try {
            // Filtrar entradas
            let [filterParamsParams, filterParamsQuery, filterParamsBody] = await Router.parseRequest(req);

            // Llamar al método pasando los datos filtrados
            let data = await MethodCallback(req, filterParamsParams, filterParamsQuery, filterParamsBody);

            // Retornar respuesta filtrando los datos con lo definido en la vista
            res.json(await Router.parseView(req, data));
        } catch (err) { //En caso de error, se retorna el error para que lo maneje la app
            next(err);
        }
    }
}
```

#### Vistas
En la carpeta __app/views__ estaran los esquemas asociados a la respuesta de los datos. Estos esquemas sirven para filtrar la informacion que se saca de la base de datos. Se debe crear una carpeta con el nombre del archivo route y dentro de ella crear tantos archivos como se desee. Todos los archivos __.json__ de la carpeta seran leidos y mezclados para hacer la documentacion y filtrar las respuetas. Los archivos deben ser JSONs con la siguiente estructura:
```
{
    "{path}": {
        "{method}": {
            "description": "Descripcion del método",
            "tags": ["Tags del método"],
            "headers": { Esquema JSON o referencia a esquema }
            "query": { Esquema JSON o referencia a esquema }
            "response": { Esquema JSON o referencia a esquema }
        },
    },
    "definitions(opcional)": {
        Esquema JSON o referencia a esquema
    }
}
```
Los objetos declarados en la seccion "definitions" de este archivo solo podrán ser usados dentro del mismo archivo.

Ademas, se puede incluir una subcarpeta __app/views/{route}/definitions__ con esquemas .json que pueden ser utilizados como referencia en cualquiera de los JSONs de vista. Todos los archivos __.json__ que se incluyan en esta ruta se mezclaran automaticamente con la sección "definitions" del esquema, y se usarán para generar la documentación de las rutas.
Para poder linkear una definicion de la carpeta "definitions" a un JSON de vista, se debe hacer usando el nombre del archivo:
```
...
    "items": { "$ref": "#/definitions/example.json" }
...
```

Se han incluido ejemplos de cada uno de estos elementos (Controladores, Modelos, Rutas y Vistas) en sus correspondientes carpetas.

#### Framework
El orden de creación de los elementos es __Modelos__, __Rutas__, __Controladores__ y finalmente las __Vistas__.
El framework permite incluir __Modelos__ dentro de __Controladores__, __Modelos__, __Vistas__ y __Controladores__ dentro de __Rutas__, y __Modelos__ dentro de __Librerias__

#### Orden de elementos para consultas
Se provee un patron para ordenar las consultas mediante el objeto query.order. Se propone al desarrollador que utilice el siguiente metodo:
```
static async getAll (req, params, query, body) {
    // Order example
    let order = [['id', 'ASC']];
    switch (query.order) {
        case "field1":
        case "field1,asc":
            order = [['field1', 'ASC']];
            break;
        case "field1,desc":
            order = [['field1', 'DESC']];
            break;
        case "custom":
            order = [['field1', 'DESC'], ['id', 'DESC']];
    }

    ...

    return await ExampleDB.example.getAll({
        where: ExampleDB.example.where(query, queryFilter),
        offset: query.offset,
        limit: query.limit,
        order: order,
    });
}
```
Las consultas de ordenaran por defecto con ```order = [['id', 'ASC']]```, y si el usuario incluye en __query.order__ alguna de los valores [__field1__, __field1,asc__, __field1,desc__] entonces se pasará el orden programado por el desarrollador.

#### Autentificacion
Para la autentificacion en el sistema, se usa [JWT](https://jwt.io).

Para la autentificación, el servidor lee este archivo __app/libs/passportExample.js__ y usa el middleware __hookJWTStrategy()__ para validar el acceso.
Si el método hookJWTStrategy logra autentificar al usuario, se agrega el __user__ dentro del objeto __req__. Por lo que el usuario logeado, sus atributos y sus métodos son accesibles dentro de los __Controladores__ accediendo al objeto __req.user__.

Para crear una ruta con autentificacion hay que incluir ```passport.authenticate('jwt', { session: false })``` en la cadena de ejecución en las __Rutas__ de este modo:
```
app/routes/example.js
class ExampleRouter extends AppRouter {
    setRoutes(router, passport) {
        ...
        router.get('/examples/me', passport.authenticate('jwt', { session: false }), this.pipe(ExampleController.getUserExamples));
        ...
    }
}
```
Esto hará que se valide el acceso mediante el header __Authorization__, el cual debe contener un __JWT__ válido. Si el __JWT__ es invalido o esta expirado, la ruta retornará un status code __401 Unauthorized__.

Se incluye una ruta de autentificacion de ejemplo en __/v1/examples/auth__ donde se puede ingresar con el usuario:
```
{
    user: {
        name: "Test",
        password: "1234"
    }
}
```
Esta ruta de autentificacion devuelve un __JWT__ válido por 30 minutos, que se puede utilizar para acceder a __/v1/examples/me__. O a las rutas que exigen __JWT__ definidas en __app/routes/example.js__.

#### Manejo de errores
Existen 3 errores definidos en el Framework: AppError, AppSchemaError y AppSequelizeError.

- __AppError(status, message)__ es el principal, y permite retornar un json con status code __status__ y un objeto { msg: __message__ } que contiene el string de error.
- __AppSchemaError(message, errors)__ es un error que ocurre al enviar un body que no cumple la especificiacion definida en el esquema de la vista, y permite retornar un json con status code 400 y un objeto { msg: __message__, errors: { __ERROR_OBJECT__ }] }.
- __AppSequelizeError(message, errors)__ es un error que ocurre al tratar de insertar o actualizar un objeto con sequelize y no cumple la validación, y este error permite retornar un json con status code 400 y un objeto { msg: __message__, errors: __ERROR_OBJECT__ ] }.

__ERROR_OBJECT__ es del tipo:
```
{
    "{FIELD}": {
        "msg": "string", //Mensaje de error
        "name": "string", //Regla aplicada
        "argument": "string" //Valores de la regla
    }
}
```

#### Acceso a la documentación autogenerada
Para generar la documentacion se usa [Swagger](https://swagger.io). El servidor genera automáticamente la documentación de las rutas con los detalles especificados en la carpeta de vistas.
Cada router genera la documentación de sus rutas y se puede acceder a esta ingresando a:
```
http://{SERVIDOR}:{PORT}/v{API_VERSION}/{ROUTER.getName()}-docs
```

En el ejemplo que se incluye, la documentación de las rutas se encuentra en
```
http://{SERVIDOR}:{PORT}/v1/example-docs
```

#### Test automaticos con mocha e integracion continua con bitbucket
Se incluye un set basico de testing en la carpeta __ci/tests__, que sirve como marco para diseñar test automatizados.
Para ejecutar los test automaticos localmente, se debe hacer con el comando:
```
npm run localtest
```

Se incluye ademas un archivo __bitbucket-pipelines.yml__ que sirve para usar ci con bitbucket. Esto permite que los test de la carpeta ci/tests se ejecuten automaticamente al hacer push en el repositorio.
El sistema usa la imagen base __rekikn/node-mainframework__. Sin embargo, se agregan nuevos modulos npm se tendra que crear una imagen propia y subirla a docker hub. Esto se puede hacer modificando el valor __services.localserver.image__ en __ci/docker-compose.yml__ reemplazandolo por ```{username}/{projectname}``` y luego ejecutando los siguientes comandos:
```
npm run localtest
docker login --username={username} --password={password}
docker push {username}/{projectname}
```
Por ultimo, para hacer un commit sin que se ejecute el ci de bitbucket, se debe hacer del siguiente modo
```
git commit -m "[skip ci] comentarios"
```

### Lista de cosas por hacer
- [x] Autoloader para Libs.
- Incluir set de librerias básicas.
- [x] Cerrar la app en caso de error grave (Vistas no creadas, errores en archivos de modelos, vista, controlador, rutas o libs).
- [x] Manejar el contexto (base path) de los routers como opcional.
- [x] Manejar 'order' en listados.
- [x] Test automaticos.
- Migraciones de DBs.
- Mejorar sistema de autentificación para que sea mas facil de usar.

### Links
- Derived from (https://github.com/bogard1/node-sequelize-jwt-mysql)
- https://github.com/dilagurung/node-sequelize-jwt-mysql
- http://docs.sequelizejs.com/manual/tutorial/models-definition.html
- https://stackoverflow.com/questions/29716346/how-to-create-a-trigger-in-sequelize-nodejs

### Lecturas aconsejable
- http://xurxodev.com/testeando-un-api-rest-con-mocha/
- https://medium.com/the-node-js-collection/making-your-node-js-work-everywhere-with-environment-variables-2da8cdf6e786
- https://alligator.io/js/async-functions/
- https://javascript.info/import-export
- https://adrianmejia.com/getting-started-with-node-js-modules-require-exports-imports-npm-and-beyond/
