-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2

--
-- DB: `exampleDB`
--

-- --------------------------------------------------------

--
-- `examples`s table estructure
--

CREATE TABLE IF NOT EXISTS `examples` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `field1` varchar(10) NOT NULL,
  `field2` varchar(10) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- `users`s table estructure
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `name` varchar(10) NOT NULL,
  `password` varchar(32) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Indexes
--

ALTER TABLE `examples`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENTs
--

ALTER TABLE `examples`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
