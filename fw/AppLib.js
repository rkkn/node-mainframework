// Base Lib.
'use strict';

class AppLib {
    static capitalize (s) {
        if (typeof s !== 'string') return '';
        return s.charAt(0).toUpperCase() + s.slice(1);
    }

    static camelcased (s) {
        if (typeof s !== 'string') return '';
        return s.replace(/s_/g, '_').replace(/_([a-z])/g, function (g) { return g[1].toUpperCase(); });
    }
}

module.exports = AppLib;
