'use strict';

const { AppController, AppError, Config } = require('../imports');
const { ExampleDB } = require('../imports').loadDBs();
const Md5 = require('md5');
const Jwt = require('jsonwebtoken');

class AuthController extends AppController {
    static async authBase (req, params, query, body) {
        //Buscar un usuario por name y password
        let user = await ExampleDB.user.findOne({ where: { name: body.user.name } });

        if (user.password !== Md5(body.user.password? body.user.password: '')) {
            throw new AppError(404, 'Authentication failed!');
        }

        let token = Jwt.sign(
            { id: user.id, mobile: user.mobile, type: 1 },
            Config.keys.secret,
            { expiresIn: '30m' }
        );

        return {
            token: `JWT ${token}`
        };
    }
}

module.exports = AuthController;
