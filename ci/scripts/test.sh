#/usr/bin/sh
cd /rest

nohup npm start > /tmp/api_output.txt & export API_PID=$!

sleep 3

npm test
RESULT=$?

echo "---------------------"
cat /tmp/api_output.txt
echo "---------------------"
echo "RESULT = $RESULT"

exit $RESULT
