// Application configuration.
'use strict';

//Load env file
require('dotenv').config();
let Config = {};

Config.port = process.env.PORT? process.env.PORT: 80;
Config.version = 1;
Config.errDefaultCode = 500;
Config.errDefaultMsg = "Something went wrong, please try again later";

//Define si los routers deben usar un basepath con su nombre
Config.routerUseBasepath = true;

Config.keys = {
    secret: process.env.SECRET,
    salt: process.env.SALT,
};

Config.swagger = {
    active: true
};

/* Database config */
Config.db = {};
Config.db.limit = 20;
/* Multi-DB support */
Config.db.exampleDB = {
    user: process.env.EXAMPLEDB_USER,
    password: process.env.EXAMPLEDB_PSWD,
    name: process.env.EXAMPLEDB_NAME,
    details: {
        host: 'localhost',
        port: 3306,
        dialect: 'mysql',
        define: { charset: 'utf8', dialectOptions: { collate: 'utf8_general_ci' }, },
        logging: (process.env.NODE_ENV !== "develop")? false: console.log,
    }
};

module.exports = Config;
