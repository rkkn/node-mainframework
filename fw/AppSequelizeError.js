'use strict';

const AppError = require('./AppError');
const ValidationError = require('sequelize').ValidationError;

class AppSequelizeError extends AppError {
    constructor(msg, errors) {
        super(400, msg);
        this.errors = {};
        for (let error of errors) {
            this.errors[error['path']] = {
                msg: error['message'],
                name: error['validatorKey'],
                argument: error['validatorName']? error['validatorName']: "",
            };
        }
    }
}

module.exports = AppSequelizeError;
