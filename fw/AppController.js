// Base Controller.
'use strict';

const Sequelize = require('sequelize');
const Op = Sequelize.Op;

class AppController {
    static likeFilter(fieldsArray, query) {
        if (!Array.isArray(fieldsArray)) {
            throw new Error("AppController.likeFilter must be called with an Array");
        }
        if (fieldsArray.length <= 0) {
            throw new Error("AppController.likeFilter can't be called with an empty Array");
        }

        if (!query.query)
            return {};

        let queryFilter = {};
        for (let element of fieldsArray) {
            queryFilter[element] = { [Op.like]: `%${query.query}%`};
        }

        //Si se inserto algun filtro
        if (Object.keys(queryFilter).length) {
            return { [Op.or]: queryFilter };
        }

        return {};
    }
}

module.exports = AppController;
