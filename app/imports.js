'use strict';
//Load FW
const Config = require('./config.js');
const Fw = require(`${__dirname}/../fw/AppFw`);
const Sequelize = require('sequelize');

//models export

module.exports = {
    //Configs
    Config: Config,

    //Framework
    FW: Fw,
    AppError: Fw.appError(),
    AppRouter: Fw.appRouter(),
    AppModel: Fw.appModel(),
    AppController: Fw.appController(),
    AppLib: Fw.appLib(),

    //Sequelize
    Sequelize: Sequelize,
    DataTypes: Sequelize.DataTypes,
    Op: Sequelize.Op,

    loadDBs: function () {
        return Fw.loadDBs();
    },

    loadControllers: function (context) {
        return Fw.loadControllers(context);
    },

    loadLibs: function () {
        return Fw.loadLibs();
    }
}
