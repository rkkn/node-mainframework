// The User model.
'use strict';

const { AppModel, Sequelize, DataTypes } = require('../../imports');

class Example extends AppModel {
    //Asociar con otros modelos aca
    static associate (Models) {
        Models.example.belongsTo(Models.user);
    }
}

/* Private Functions Section */

/* Model definition */
const ModelDefinition = {
    id: {
        type: Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement: true,
    },

    /*user_id: {
        type: Sequelize.INTEGER,
    },*/

    field1: {
        type: Sequelize.STRING,
        allowNull: false,
    },

    field2: {
        type: Sequelize.STRING,
        allowNull: false,
    },

    modified: DataTypes.DATE,
    created: DataTypes.DATE,
};

const ModelOptions = {
    modelName: 'example',

    //underscored: true indicates the the column names of the database tables are snake_case rather than camelCase.
    underscored: true,

    //Renombrar timestamps
    updatedAt: 'modified',
    createdAt: 'created'
};

/* Model Init */
module.exports = function (SequelizeInstance, DataTypes) {
    return Example.init(ModelDefinition, Object.assign({ sequelize: SequelizeInstance }, ModelOptions));
}
