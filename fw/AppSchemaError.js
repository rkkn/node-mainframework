'use strict';

const AppError = require('./AppError');
const ValidationError = require('sequelize').ValidationError;

class AppSchemaError extends AppError {
    constructor(msg, errors) {
        super(400, msg);
        this.errors = {};
        for (let error of errors) {
            this.errors[error['property'].replace('instance.','')] = {
                msg: error['stack'].replace('instance.',''),
                name: error['name'],
                argument: error['argument']? error['argument']: "",
            };
        }
    }
}

module.exports = AppSchemaError;
