// The User model.
'use strict';

const { AppModel, Sequelize, DataTypes } = require('../../imports');

class User extends AppModel {
    //Asociar con otros modelos aca
    static associate (Models) {
        Models.user.hasMany(Models.example);
    }

    getExamples(options) {
        return this.sequelize.models.example.getAll({
            include: [{
                model: this.sequelize.models.user,
                where: this.sequelize.models.user.where(),
            }],
            where: options.where,
            offset: options.offset,
            limit: options.limit,
        });
    }
}

/* Private Functions Section */

/* Model definition */
const ModelDefinition = {
    id: {
        type: Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement: true,
    },

    name: {
        type: Sequelize.STRING,
        allowNull: false,
    },

    password: {
        type: Sequelize.STRING,
        allowNull: false,
    },

    modified: DataTypes.DATE,
    created: DataTypes.DATE,
};

const ModelOptions = {
    modelName: 'user',

    //underscored: true indicates the the column names of the database tables are snake_case rather than camelCase.
    underscored: true,

    //Renombrar timestamps
    updatedAt: 'modified',
    createdAt: 'created'
};

/* Model Init */
module.exports = function (SequelizeInstance, DataTypes) {
    return User.init(ModelDefinition, Object.assign({ sequelize: SequelizeInstance }, ModelOptions));
}
