// Base Controller.
'use strict';

const Fs = require('fs');
const Path = require('path');
const Filter = require('json-schema-filter');
const Parser = require('json-schema-ref-parser');
const Merger = require('json-schema-merge-allof');
const JsonValidate = require('jsonschema').validate;
const Op = require('sequelize').Op

const Config = require('../app/config');
const AppError = require('./AppError');
const AppSchemaError = require('./AppSchemaError');

class AppRouter {
    constructor (app, passport) {
        //Load router
        this.router = require('express').Router();

        this.lazyLoad = new Promise(async (resolve, reject) => {
            try {
                // Rutas parseadas. Archivos .json en la carpeta views/{route} pero que han reemplazado las referencias por el objeto correspondiente
                let routesDefinitions = {};
                // Definiciones base. Archivos .json en la carpeta views/{route}/definitions
                let baseDefinitions = {};

                // Rutas sin parsear. Archivos .json en la carpeta views/{route} pero sin reemplazar las referencias
                let routesSwagger = {};
                // Referencias de rutas. Referencias internas en los archivos .json en la carpeta views/{route}
                let routesSwaggerDefinitions = {};

                // Check if folders are created
                checkPaths(this.getName());

                // Load View Schemas Definitions
                let baseSchemas = loadBaseDefinitions(this.getName());
                for (const schema of baseSchemas) {
                    Object.assign(baseDefinitions, schema);
                }

                // Load View Schemas
                let viewSchemas = loadPathDefinitions(this.getName());

                for (const schema of viewSchemas) {
                    //Asignar el schema a los objetos de Swagger, para utilizarlos en la documentacion
                    Object.assign(routesSwagger, schema);
                    Object.assign(routesSwaggerDefinitions, schema.definitions);

                    //Crear el schema mezclando las definiciones locales (en el archivo) con las globales (en la carpeta definitions)
                    Object.assign(schema, { definitions: Object.assign({}, baseDefinitions, schema.definitions) });

                    //Parsear schema reemplazando las referencias por el objeto referenciado
                    let parsedSchema = await Parser.dereference(schema);
                    let mergedSchema = Object.assign({}, parsedSchema);

                    //Parsear allOf en cada uno de los elementos del schema
                    for (let route in mergedSchema) {
                        if (route == "definitions") {
                            //Parsear schema reemplazando allOf por los sub-schemas mergeados
                            mergedSchema[route] = await Merger(mergedSchema[route]);
                            continue;
                        }

                        for (let method in mergedSchema[route]) {
                            if (mergedSchema[route][method]["headers"])
                                mergedSchema[route][method]["headers"] = await Merger(mergedSchema[route][method]["headers"]);
                            if (mergedSchema[route][method]["query"])
                                mergedSchema[route][method]["query"] = await Merger(mergedSchema[route][method]["query"]);
                            if (mergedSchema[route][method]["body"])
                                mergedSchema[route][method]["body"] = await Merger(mergedSchema[route][method]["body"]);
                            if (mergedSchema[route][method]["response"])
                                mergedSchema[route][method]["response"] = await Merger(mergedSchema[route][method]["response"]);
                        }
                    }

                    //Mezclar todo en routesDefinitions
                    Object.assign(routesDefinitions, mergedSchema);
                }

                this.routesSwagger = routesSwagger;
                this.routesSwaggerDefinitions = routesSwaggerDefinitions;
                this.baseDefinitions = baseDefinitions;
                this.routesDefinitions = routesDefinitions;

                // Load Routes
                this.setRoutes(this.router, passport);

                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    getName () {
        return this.constructor.name.replace('Router', '').toLowerCase();
    }

    getBasePath () {
        if (Config.routerUseBasepath === false)
            return '/';
        return `/v${Config.version}/${this.getName()}`;
    }

    getSwaggerPath () {
        if (Config.routerUseBasepath === false)
            return `/${this.getName()}-docs`;
        return `/v${Config.version}/${this.getName()}-docs`
    }

    setRoutes (router, passport) {
        throw new Error(`You must implement setRoutes(router, passport) function in '${this.constructor.name}'`);
    }

    getRoutes () {
        return this.router;
    }

    /* Paths functions */
    static appDir (pathToAdd) {
        if (typeof pathToAdd !== "string") {
            pathToAdd = "";
        }
        return Path.join(__dirname, '../app', pathToAdd);
    }

    static libDir (pathToAdd) {
        if (typeof pathToAdd !== "string") {
            pathToAdd = "";
        }
        return Path.join(__dirname, '../app/lib', pathToAdd);
    }

    static modelDir (pathToAdd) {
        if (typeof pathToAdd !== "string") {
            pathToAdd = "";
        }
        return Path.join(__dirname, '../app/models', pathToAdd);
    }

    static controllerDir (pathToAdd) {
        if (typeof pathToAdd !== "string") {
            pathToAdd = "";
        }
        return Path.join(__dirname, '../app/controllers', pathToAdd);
    }

    static viewDir (pathToAdd) {
        if (typeof pathToAdd !== "string") {
            pathToAdd = "";
        }
        return Path.join(__dirname, '../app/views', pathToAdd);
    }

    static assetDir (pathToAdd) {
        if (typeof pathToAdd !== "string") {
            pathToAdd = "";
        }
        return Path.join(__dirname, '../app/assets', pathToAdd);
    }

    /* Route - View/Schema parse functions */
    pipe (MethodCallback) {
        let Router = this;
        return async function (req, res, next) {
            // Marcar respuestas como JSON
            res.setHeader('Content-Type', 'application/json');

            try {
                // Filtrar entradas
                let [filterParamsParams, filterParamsQuery, filterParamsBody] = await Router.parseRequest(req);

                // Llamar al método pasando los datos filtrados
                let data = await MethodCallback(req, filterParamsParams, filterParamsQuery, filterParamsBody);

                // Retornar respuesta filtrando los datos con lo definido en la vista
                res.json({
                    data: await Router.parseView(req, data)
                });
            } catch (err) { //En caso de error, se retorna el error para que lo maneje la app
                next(err);
            }
        }
    }

    wrapper (MethodCallback) {
        let Router = this;
        return async function (req, res, next) {
            try {
                // Filtrar entradas
                let [filterParamsParams, filterParamsQuery, filterParamsBody] = await Router.parseRequest(req);

                // Llamar al método pasando los datos filtrados
                await MethodCallback(req, filterParamsParams, filterParamsQuery, filterParamsBody, res);
            } catch (err) { //En caso de error, se retorna el error para que lo maneje la app
                next(err);
            }
        }
    }

    async parseRequest(req) {
        //Filtrar elementos de entrada
        return [req.params, await this.parseQuery(req), await this.parseBody(req)]
    }

    async parseQuery(req) {
        //Obtener schema de entrada (query)
        let schema = this.getRouteDefinitions(req)['query'];

        if (!schema) {
            return {};
        }

        //Filtrar elementos de entrada (query)
        return deepQueryFilter(schema, await parseSchema(schema, req.query));
    }

    async parseBody(req) {
        //Obtener schema de entrada (body)
        let schema = this.getRouteDefinitions(req)['body'];

        if (!schema) {
            return {};
        }

        //Filtrar elementos de entrada (body)
        let parsedBody = await parseSchema(schema, req.body);
        let validateBody = JsonValidate(parsedBody, schema);

        //Validar elementos requeridos
        if (validateBody.errors.length > 0)
            throw new AppSchemaError("Body Format Error", validateBody.errors);

        return parsedBody;
    }

    async parseView(req, data) {
        //Obtener schema de salida de datos
        let schema = this.getRouteDefinitions(req)['response'];

        if (!schema) {
            throw new Error(`Route method result schema for '${req.route.path}' not found`);
        }

        //Parsear elementos de salida
        return await parseSchema(schema, data);
    }

    getRouteDefinitions(req) {
        //Obtener nombre del Router
        let path = req.route.path;
        let method = req.method.toLowerCase();

        if (!req || !path || !method) {
            throw new Error("Error in req, path or method. (Router implementation)");
        }

        if (!this.routesDefinitions) {
            throw new Error("View implementation not loaded yet");
        }

        if (!this.routesDefinitions[path]) {
            throw new Error(`Route schema for '${req.route.path}' not found`);
        }

        if (!this.routesDefinitions[path][method]) {
            throw new Error(`Route method schema for '${req.route.path}' not found`);
        }

        return this.routesDefinitions[path][method];
    }

    async swaggerDoc() {
        let route = this.getName();
        let paths = {};
        let server = require('os').hostname();

        for (const route in this.routesSwagger) {
            let routeSwagger = route.replace(/\/:(\w+)\//mg, "\/{$1}\/").replace(/\/:(\w+)$/mg, "\/{$1}");
            if (!paths[routeSwagger])
                paths[routeSwagger] = {};

            //Saltarse las definiciones
            if (route == "definitions")
                continue;

            for (const method in this.routesDefinitions[route]) {
                //Descripcion del metodo
                paths[routeSwagger][method] = {
                    description: this.routesDefinitions[route][method]['description'],
                    tags: this.routesDefinitions[route][method]['tags'],
                    parameters: [],
                }

                //Headers
                //Los headers pueden ser una referencia, así que hay que parsear el schema con las referencias internas
                let headers = {};
                if (this.routesDefinitions[route][method]['headers'])
                    headers = await Parser.dereference(Object.assign({}, this.routesDefinitions[route][method]['headers'], { definitions: this.routesSwaggerDefinitions }));
                for(const header in headers['properties']) {
                    paths[routeSwagger][method]['parameters'].push(Object.assign({}, headers['properties'][header], {
                        "name": header,
                        "in": "header",
                    }));
                }

                //Params
                //Los params pueden ser una referencia, así que hay que parsear el schema con las referencias internas
                let params = {};
                if (this.routesDefinitions[route][method]['params'])
                    params = await Parser.dereference(Object.assign({}, this.routesDefinitions[route][method]['params'], { definitions: this.routesSwaggerDefinitions }));
                for(const param in params['properties']) {
                    paths[routeSwagger][method]['parameters'].push(Object.assign({}, params['properties'][param], {
                        "name": param,
                        "in": "path",
                        "required": true,
                    }));
                }

                //Query
                //El objeto query pueden ser una referencia, así que hay que parsear el schema con las referencias internas
                let queries = {};
                if (this.routesDefinitions[route][method]['query'])
                    queries = await Parser.dereference(Object.assign({}, this.routesDefinitions[route][method]['query'], { definitions: this.routesSwaggerDefinitions }));
                for(const query in queries['properties']) {
                    paths[routeSwagger][method]['parameters'].push(Object.assign({}, queries['properties'][query], {
                        "name": query,
                        "in": "query",
                        "required": false,
                    }));
                }

                //Body
                //El objeto body pueden ser una referencia, así que hay que parsear el schema con las referencias internas
                if (this.routesDefinitions[route][method]['body']){
                    let body = await Parser.dereference(Object.assign({}, this.routesDefinitions[route][method]['body'], { definitions: this.routesSwaggerDefinitions }));
                    paths[routeSwagger][method]['parameters'].push({
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": body
                    });
                }

                //Response
                paths[routeSwagger][method]['responses'] = {
                    "200": {
                        "description": "Success",
                        "schema": {
                            "type": "object",
                            "properties": {
                                "data": this.routesDefinitions[route][method]['response']
                            }
                        }
                    },
                    "default": {
                        "description": "Error",
                        "schema": {
                            "type": "object",
                            "properties": {
                                "msg": {
                                    "type": "string"
                                },
                                "errors": {
                                    "type": "object"
                                }
                            }
                        }
                    }
                }
            }
        }



        return {
            "swagger": "2.0",
            "info": {
                "version": "0.0.2",
                "title": `Safecard ${route} API`
            },
            "host": (Config.port == 80 || Config.port == 443)? `${server}`: `${server}:${Config.port}`,
            "basePath": this.getBasePath(),
            "schemes": [
                "http"
            ],
            "consumes": [
                "application/json"
            ],
            "produces": [
                "application/json"
            ],
            "paths": paths,
            "definitions": this.baseDefinitions,
        };
    }
}

function checkPaths(routerName) {
    let path;

    // Check if folders exists
    path = Path.join(AppRouter.viewDir(routerName));
    if (!Fs.statSync(path))
        throw new Error(`You must create '${path}' folder`);

    // Check if folders exists
    path = Path.join(AppRouter.viewDir(routerName), 'definitions');
    if (!Fs.statSync(path))
        throw new Error(`You must create '${path}' folder`);
}

function loadBaseDefinitions(routerName){
    let path = Path.join(AppRouter.viewDir(routerName), 'definitions');

    return Fs
    .readdirSync(path)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file.slice(-5) === '.json');
    }).map(file => {
        return { [file]: require(Path.join(path, file)) };
    });
}

function loadPathDefinitions(routerName){
    let path = AppRouter.viewDir(routerName);

    return Fs
    .readdirSync(path)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file.slice(-5) === '.json');
    }).map(file => {
        return require(Path.join(path, file));
    });
}

async function parseSchema (schema, object) {
    return Filter(schema, deepReplaceNull(object));
}

function deepReplaceNull(object) {
    //La version instalada tiene soporte para nulls y emtpy strings. Con esto se reemplazan los nulls
    //return JSON.parse(JSON.stringify(object, function replacer(key, value){ return (value === null || typeof value === "undefined")? "": value }));
    return object;
}

// Se usa para permitir operadores de busqueda en la query de las rutas
function deepQueryFilter(schema, query){
    let where = {};

    // Procesar funciones en busquedas
    for (let field in query) {
        if (field == "query" || field == "page" || field == "limit")
            continue;

        let value = getFormatedField(query[field]);
        if (typeof value != "undefined") {
            where[field] = value;
        }
    }

    // Procesar objetos especiales de busqueda
    // Query or search
    if (query["query"] && typeof query["query"] === "string") {
        where["query"] = query["query"];
    }

    // Pagination
    if (query["limit"]) {
        let limit = parseInt(query["limit"]);
        if(!isNaN(limit) && limit > 0) {
            where["limit"] = limit;
        } else if (schema["properties"]["limit"]["default"]) {
            where["limit"] = schema["properties"]["limit"]["default"];
        }
    } else if (schema["properties"]["limit"]["default"]) {
        where["limit"] = schema["properties"]["limit"]["default"];
    }

    if (where["limit"]) {
        let page = parseInt(query["page"]);
        if (!isNaN(page) && page > 0) {
            where["offset"] = page * where["limit"];
        } else if (where["limit"]) { //Si existe un default en el schema, asignar ese
            where["offset"] = 0;
        }
    }

    return where;
}

function getFormatedField(fieldValue){
    if (fieldValue === null)
        return null;
    if (fieldValue === undefined)
        return undefined
    if (typeof fieldValue === 'string')
        return fieldValue;
        if (typeof fieldValue === 'integer')
            return fieldValue;
    if (fieldValue.constructor === ({}).constructor) {
        let response = {};
        let validJson = 0;
        for (let operator in fieldValue) {
            if (typeof fieldValue[operator] !== 'string')
                continue;
            validJson += parseSeqOperator(response, operator, fieldValue[operator]);
        }
        if (validJson > 0) {
            return response;
        }
    }

    return undefined;
}

function parseSeqOperator(response, operator, value) {
    switch (operator) {
        case "gt":
             Object.assign(response, { [Op.gt]: value });
            return 1;
        case "gte":
            Object.assign(response, { [Op.gte]: value });
            return 1;
        case "lt":
            Object.assign(response, { [Op.lt]: value });
            return 1;
        case "lte":
            Object.assign(response, { [Op.lte]: value });
            return 1;
        case "ne":
            Object.assign(response, { [Op.ne]: value });
            return 1;
        case "eq":
            Object.assign(response, { [Op.eq]: value });
            return 1;
        case "not":
            Object.assign(response, { [Op.not]: value });
            return 1;
        case "like":
            Object.assign(response, { [Op.like]: value });
            return 1;
        case "notLike":
            Object.assign(response, { [Op.notLike]: value });
            return 1;
        case "iLike":
            Object.assign(response, { [Op.iLike]: value });
            return 1;
        case "notILike":
            Object.assign(response, { [Op.notILike]: value });
            return 1;
        case "startsWith":
            Object.assign(response, { [Op.startsWith]: value });
            return 1;
        case "endsWith":
            Object.assign(response, { [Op.endsWith]: value });
            return 1;
        case "substring":
            Object.assign(response, { [Op.substring]: value });
            return 1;
    }
    return undefined;
}

module.exports = AppRouter;
