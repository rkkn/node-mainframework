--
-- Data insert for `examples` table
--

INSERT INTO `examples` (`id`, `user_id`, `field1`, `field2`, `created`, `modified`) VALUES
(1, NULL, 'value1', 'value2', '2019-08-29 19:32:12', '2019-08-29 19:32:12'),
(2, 1, 'value3', 'value4', '2019-08-29 19:32:12', '2019-08-29 19:32:12'),
(3, 2, 'value5', 'value6', '2019-08-29 19:32:12', '2019-08-29 19:32:12'),
(4, 1, 'value7', 'value8', '2019-08-29 19:32:12', '2019-08-29 19:32:12');


--
-- Data insert for `users` table
--

INSERT INTO `users` (`id`, `name`, `password`, `created`, `modified`) VALUES
(1, 'Test', '81dc9bdb52d04dc20036dbd8313ed055', '2019-08-29 19:32:12', '2019-08-29 19:32:12'),
(2, 'Test2', 'd93591bdf7860e1e4ee2fca799911215', '2019-08-30 11:56:24', '2019-08-30 11:56:24');
