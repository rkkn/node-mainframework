// AppFw.
'use strict';

const Fs = require('fs');
const Path = require('path');//Load Config
const SwaggerUi = require('swagger-ui-express');
const Sequelize = require('sequelize');

const Config = require('../app/config');

/* Error handling */
const AppError = require('./AppError');
const AppSchemaError = require('./AppSchemaError');
const AppSequelizeError = require('./AppSequelizeError');

/* Utils */
const AppLib = require('./AppLib');

class AppFw {
    constructor (app) {
        this.app = app;
    }

    /* Return base objects functions */
    static appError () {
        return require('./AppError');
    }

    static appRouter () {
        return require('./AppRouter');
    }

    static appController () {
        return require('./AppController');
    }

    static appModel () {
        return require('./AppModel');
    }

    static appLib () {
        return require('./AppLib');
    }

    /* Load functions */
    //Funcion que lee todos los objectos en app/models. Deben estar en la carpeta con el nombre de la BD
    static loadDBs () {
        //Return object
        let DB = {};

        let [files, folders] = [
            loadFiles(Path.join(__dirname, '../app/models')),
            loadFolders(Path.join(__dirname, '../app/models'))
        ];

        if (files.length > 0) {
            //Single database support
            let sequelize = new Sequelize(
                Config.db.name,
                Config.db.user,
                Config.db.password,
                Config.db.details,
            );

            DB['Default'] = {
                sequelize: sequelize,
                Sequelize: Sequelize,
            };

            //Load models
            for(let file of files) {
                let model = sequelize['import'](Path.join(__dirname, '../app/models', file));
                DB['Default'][model.name] = model;
            }

            //Asociate models
            Object.keys(DB['default']).forEach(modelName => {
                if (DB['Default'][modelName].associate && typeof DB['default'][modelName].associate == "function") {
                    DB['Default'][modelName].associate(DB['Default']);
                }
            });
        }

        for(let folder of folders) {
            let subfolderFiles = loadFiles(Path.join(__dirname, '../app/models', folder));
            if (!subfolderFiles || subfolderFiles.length <= 0)
                continue;

            let cpFolder = AppLib.capitalize(folder);

            let sequelize = new Sequelize(
                Config.db[folder].name,
                Config.db[folder].user,
                Config.db[folder].password,
                Config.db[folder].details,
            );

            DB[cpFolder] = {
                sequelize: sequelize,
                Sequelize: Sequelize,
            };

            //Load models
            for(let file of subfolderFiles) {
                let model = sequelize['import'](Path.join(__dirname, '../app/models', folder, file));
                DB[cpFolder][model.name] = model;
            }

            //Asociate models
            Object.keys(DB[cpFolder]).forEach(modelName => {
                if (DB[cpFolder][modelName].associate && typeof DB[cpFolder][modelName].associate == "function") {
                    DB[cpFolder][modelName].associate(DB[cpFolder]);
                }
            });
        }

        return DB;
    }

    static loadControllers (context) {
        //Return object
        let Controllers = {};

        //Leer la carpeta app/controllers
        let files = loadFiles(Path.join(__dirname, '../app/controllers'));
        let path = Path.join(__dirname, '../app/controllers');

        //Load controllers
        for(let file of files) {
            Controllers[AppLib.capitalize(file)] = require(Path.join(path, file));
        }

        //Leer la carpeta app/controllers/${context}
        if (typeof context === "string" && context) {
            files = loadFiles(Path.join(__dirname, '../app/controllers', context));
            path = Path.join(__dirname, '../app/controllers', context);

            //Load controllers
            for(let file of files) {
                Controllers[AppLib.capitalize(file)] = require(Path.join(path, file));
            }
        }

        return Controllers;
    }

    static loadLibs () {
        //Return object
        let Libs = {};

        let files = loadFiles(Path.join(__dirname, '../app/libs'));

        //Load controllers
        for(let file of files) {
            Libs[AppLib.capitalize(file)] = require(Path.join(__dirname, '../app/libs', file));
        }

        return Libs;
    }

    //Funcion que lee todas las rutas
    static async loadRoutes (app, passport) {
        let routes = loadFiles(Path.join(__dirname, '../app/routes'));
        for (const file of routes) {
            let router = new (require(Path.join(__dirname, '../app/routes', file)))(app, passport)
            // Esperar que los schemas sean leidos
            let loaded = await router.lazyLoad;

            //Linkear rutas
            app.use(router.getBasePath(), router.getRoutes());

            //Si la documentacion de swagger no esta activa. retornar
            if (Config.swagger.active) {
                const useSchema = schema => (...args) => SwaggerUi.setup(schema)(...args);
                app.use(router.getSwaggerPath(), SwaggerUi.serve, useSchema(await router.swaggerDoc(), false, { 'validatorUrl' : null }));
            }
        }
    }

    static handleError(res, err){
        if (typeof err !== "undefined" && err.constructor !== "undefined") {
            switch (err.constructor) {
                case AppSchemaError:
                case AppSequelizeError:
                    return res.status(err.status).json({ msg: err.msg, errors: err.errors });
                case AppError:
                    return res.status(err.status).json({ msg: err.msg })
            }
        }

        console.log(err.stack);
        res.status(Config.errDefaultCode).json({ msg: Config.errDefaultMsg });
    }
}

//Retorna los archivos de una carpeta
function loadFolders(path) {
    return Fs.readdirSync(path).filter(function (file) {
        return Fs.statSync(path + '/' + file).isDirectory();
    });
}

function loadFiles(path) {
    return Fs.readdirSync(path).filter(function (file) {
        return !Fs.statSync(path + '/' + file).isDirectory() && (file.indexOf('.') !== 0) && (file.slice(-3) === '.js');
    }).map(file => {
        return file.replace('.js','');
    });
}

module.exports = AppFw;
