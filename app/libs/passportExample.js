'use strict';

// JWT
const { Strategy, ExtractJwt } = require('passport-jwt');
// Leer configuraciones
const { Config } = require('../imports');
// Conexion a DB
const { ExampleDB } = require('../imports').loadDBs();

// Hooks the JWT Strategy.
function hookJWTStrategy(passport, options) {
    var defaultOptions = {
        secretOrKey: Config.keys.secret,
        jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("jwt"),
        ignoreExpiration: false,
    };

    passport.use(new Strategy(Object.assign({}, defaultOptions, options), function(JWTPayload, callback){
        ExampleDB.user.findOne({ where: { id: JWTPayload.id } })
        .then(function(user){
            if(!user) {
                return callback(null, false);
            }

            callback(null, user);
        }).catch(function (error) {
            callback(error, false);
        });
    }));
}

module.exports = hookJWTStrategy;
