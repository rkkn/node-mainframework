'use strict';

// Conexion a DB
const { AppLib } = require('../imports');

class ExampleLib extends AppLib {
    static isInt (v) {
        if (isNaN(parseInt(v)))
            return false;
        return Number.isInteger(parseInt(v))
    }
}

module.exports = ExampleLib;
